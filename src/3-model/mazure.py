import numpy as np
import xarray as xr
from scipy.special import k0
from scipy.linalg import sqrtm, expm

path_system_matrix = "data/2-interim/system_matrix.nc"
path_kdc = "data/2-interim/kdc.nc"

A = xr.open_dataarray(path_system_matrix, chunks={"x":100, "y":100})
kdc = xr.open_dataset(path_kdc, chunks={"x":100, "y":100})
kdi = kdc["kd"].isel(x=500,y=500).compute()
ci = kdc["c"].isel(x=500,y=500).compute()
Ai = A.isel(x=500,y=500).compute()

def system_matrix(kd, c):
    """Create system matrix for kD and c. Assume float vectors along layer"""
    a = 1./(kd*c)
    c2 = np.roll(c,-1,0)
    c2[-1] = np.inf
    b = 1/(kd*c2)
    return np.diag(a+b)-np.diag(a[1:], -1)-np.diag(b[:-1],1)


kdi = np.array([100.,200,300,400])
ci = np.array([500.,600,700,800])
Ai = system_matrix(kdi,ci)

r = 100
#Q = np.array([100,0,0,0,0,0,0])
Q = np.array([0,0,0,1200])

def mazure(x, h):
    return np.matmul(expm(-x*sqrtm(Ai)),h)