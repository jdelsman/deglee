import numpy as np
import xarray as xr
from scipy.special import k0
from scipy.linalg import sqrtm, eigh
from tqdm import tqdm
import numba
from numba import prange, njit, jit, objmode
import matplotlib.pyplot as plt

path_system_matrix = "data/2-interim/system_matrix.nc"
path_sqA = "data/2-interim/sqA.nc"
path_phi = "data/2-interim/phi.nc"
path_kdc = "data/2-interim/kdc.nc"

A = xr.open_dataarray(path_system_matrix, chunks={"x":100, "y":100})
kdc = xr.open_dataset(path_kdc, chunks={"x":100, "y":100})
kd = kdc["kd"].sel(x=slice(12_500,100_000),y=slice(425_000,355_000))#.compute()  .isel(x=slice(500,510),y=slice(500,510)).compute()#
c = kdc["c"].isel(x=slice(500,510),y=slice(500,510)).compute()##.isel(x=slice(500,510),y=slice(500,510)).compute()
A = A.sel(x=slice(12_500,100_000),y=slice(425_000,355_000))#.compute().isel(x=slice(500,510),y=slice(500,510)).compute()#

#has_data = ~np.any(np.isnan(A),axis=(0,1))
kdi = kd#.where(kd.notnull())
#ci = c.where(kd.notnull())
Ai = A#.where(kd.sel(layer=2).notnull())
Ai = A.isel(x=slice(90,100),y=slice(90,100)).compute()


def system_matrix(kd, c):
    """Create system matrix for kD and c. Assume float vectors along layer"""
    a = 1./(kd*c)
    c2 = np.roll(c,-1,0)
    c2[-1] = np.inf
    b = 1/(kd*c2)
    return np.diag(a+b)-np.diag(a[1:], -1)-np.diag(b[:-1],1)

def K0(A):
    """Matrix implementation of Bessel function k0
    A is a square matrix
    from Notes on https://docs.scipy.org/doc/scipy/reference/generated/scipy.linalg.funm.html"""
    w, v = eigh(A, check_finite=True)
    ## if you further know that your matrix is positive semidefinite,
    ## you can optionally guard against precision errors by doing
    w = np.maximum(w, 1e-9)
    w = k0(w)
    return (v * w).dot(v.conj().T)

def numba_eigh(A):
    v = np.full_like(A, np.nan)
    w = np.full_like(A[0], np.nan)
    ny,nx = A.shape[2:]
    for i in prange(ny):
        for j in prange(nx):
            if np.any(np.isnan(A[:,:,i,j])|np.isinf(A[:,:,i,j])):
                continue
            else:
                w[:,i,j], v[:,:,i,j] = np.linalg.eigh(A[:,:,i,j])
    return w, v            
njit_eigh = njit(numba_eigh)            

def numba_matmul(w, v):
    ny,nx = v.shape[2:]
    res = np.full_like(v, np.nan)
    for i in prange(ny):
        for j in prange(nx):
            vi = v[:,:,i,j]
            wi = w[:,i,j]
            res[:,:,i,j] = (vi * wi).dot(vi.conj().T)
    return res
njit_matmul = njit(numba_matmul)            

def numba_K0(A):
    w, v = njit_eigh(A)
    w = np.maximum(w, 1e-9)
    w = k0(w)
    return njit_matmul(w, v)

def numba_sqrtm(A):
    ny,nx = A.shape[2:]
    res = np.full_like(A, np.nan)
    for i in prange(ny):
        for j in prange(nx):
            if np.any(np.isnan(A[:,:,i,j])|np.isinf(A[:,:,i,j])):
                continue
            with objmode(s='float64[:,:]'):
                s = sqrtm(A[:,:,i,j])
            res[:,:,i,j] = s
    return res
njit_sqrtm =njit(numba_sqrtm, parallel=True)
jit_sqrtm = jit(numba_sqrtm, nopython=False, parallel=True, forceobj=False, locals={'sqrtm':numba.types.float64[:,:](numba.types.float64[:,:])}) 

def loop_sqrtm(A):
    ny,nx = A.shape[2:]
    res = np.full_like(A, np.nan)
    for i in tqdm(range(ny)):
        for j in tqdm(range(nx), leave=False):
            if np.any(np.isnan(A[:,:,i,j])|np.isinf(A[:,:,i,j])):
                continue
            res[:,:,i,j] = sqrtm(A[:,:,i,j])
    return res

# kdi = np.array([100.,200,300,400])
# ci = np.array([500.,600,700,800])
# Ai = system_matrix(kdi,ci)

# r = 100
# Q = np.array([1e3,0,0,0,0,0,0])
# Q = np.array([0,0,0,1200])

def deglee_K0(r, sqA):
    return numba_K0(r * sqA)

def numba_deglee(k0rA, kd, Q):
    ny,nx = kd.shape[1:]
    phi = np.full_like(kd, np.nan)
    for i in prange(ny):
        for j in prange(nx):
            if np.any(np.isnan(k0rA[:,:,i,j])|np.isinf(k0rA[:,:,i,j])):
                continue
            phi[:,i,j] = 1/(2*np.pi) * k0rA[:,:,i,j] @ (Q / kd[:,i,j])
    return phi
njit_deglee = njit(numba_deglee)

def deglee(r, kd, c, Q):
    A = system_matrix(kd, c)
    return 1/(2*np.pi) * K0(r * sqrtm(A)) @ (Q/kd)  # @ is matrix multiplication

try:
    sqA = xr.open_dataarray(path_sqA)
except FileNotFoundError:
    sqA = loop_sqrtm(A.values)  # njit crashes :-(  
    sqA = A.copy(data=sqA)
    sqA.to_netcdf(path_sqA)
r = 100.
k0rA = deglee_K0(r, sqA.values)

# Q = np.array([-1e6,0,0,0,0,0,0])
# phi = njit_deglee(k0rA, kd.values, Q)
# phi = kd.copy(data=phi)
# phi.name = "phi"
# phi.to_netcdf(path_phi)
# phi.plot(col="layer", col_wrap=3)
# plt.show()

phis = []
for qlay in range(8):
    Q = np.array([0.,0,0,0,0,0,0])
    Q[qlay] = -1e6 / 365.
    phi = njit_deglee(k0rA, kd.values, Q)
    phi = kd.copy(data=phi)
    phi.name = "phi"
    phis.append(phi)
phis = xr.concat(phis, dim="qlayer")
phis = phis.assign_coords({"qlayer":list(range(2,9)), "layer":list(range(2,9))})
phis.to_netcdf(path_phi)