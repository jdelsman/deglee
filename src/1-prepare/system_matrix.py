from operator import ilshift
import xarray as xr
import numpy as np
# import dask
# from numba import njit, prange

path_kdc = "data/2-interim/kdc.nc"
path_system_matrix = "data/2-interim/system_matrix.nc"
kdc = xr.open_dataset(path_kdc, chunks={"x":100, "y":100})

kd = kdc["kd"]#.isel(x=slice(500,510),y=slice(500,510))#.compute()
c = kdc["c"]#.isel(x=slice(500,510),y=slice(500,510))#.compute()
kd = np.array([100.,200,300,400])
c = np.array([500.,600,700,800])

def system_matrix(kd, c):
    """Create system matrix for kD and c. Assume float vectors along layer"""
    a = 1./(kd*c)
    c2 = np.roll(c,-1,0)
    c2[-1] = np.inf
    b = 1/(kd*c2)
    return np.diag(a+b)-np.diag(a[1:], -1)-np.diag(b[:-1],1)
#system_matrix2 = njit(system_matrix)
# super slow:
# A = np.zeros((kd.shape[0],kd.shape[0],*kd.shape[1:]))
# for i in range(kd.shape[1]):
#     for j in range(kd.shape[2]):
#         A[:,:,i,j] = system_matrix(kd.values[:,i,j],c.values[:,i,j])

# much faster:
def system_matrix2(kd, c):
    """Create system matrix for kD and c. Assume float arrays with layer as the first axis"""
    A = np.zeros((kd.shape[0],kd.shape[0],*kd.shape[1:]))
    n = len(kd)
    a = 1./(kd*c)
    c2 = c.copy()
    c2[:-1] = c[1:]
    c2[-1] = np.inf
    b = 1/(kd*c2)
    for i in range(n):
        A[i,i] = a[i] + b[i]
        if i:
            A[i,i-1] = -a[i]
            A[i-1,i] = -b[i-1]
    return A
        
A = system_matrix2(kd.values,c.values)
da = xr.DataArray(A,coords={"layer1":kd.layer.values,"layer2":kd.layer.values,"y":kd.y,"x":kd.x},dims=["layer1","layer2","y","x"])
da.to_netcdf(path_system_matrix)