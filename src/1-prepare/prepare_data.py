import xarray as xr
import imod

path_kd = "data/1-external/kd/*.idf"
path_c = "data/1-external/c/*.idf"
path_kdc = "data/2-interim/kdc.nc"

kd = imod.idf.open(path_kd)
c = imod.idf.open(path_c)

# move kD one higher, so layer 1 is first confined layer, with overlying c
kd = kd.shift(layer=-1).sel(layer=slice(1,7))

kdc = xr.Dataset({"kd":kd,"c":c})
kdc.to_netcdf(path_kdc)